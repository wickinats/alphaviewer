# AlphaViewer

[![forthebadge](https://forthebadge.com/images/badges/made-with-vue.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/designed-in-ms-paint.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/for-you.svg)](https://forthebadge.com)

AlphaViewer is a crossplatform application for view and use wallpaper from alphacoders.com.

## Build:
```bash
# Clone repository
$ git clone https://github.com/Wickinats/AlphaViewer.git
# Change folder
$ cd AlphaViewer
# Install dependencies
$ npm install
# Run Application
$ npm run electron

```

## Tasks:

- [x] Create a interactive gui-interface with VueJS.
- [x] Create a simple ElectronJs scripts.
- [x] Create NodeJS scripts for a working with API of alphacoders.com.
- [ ] Create NodeJS scripts for working with filesystem on different platforms.
- [ ] Catch the platypus perry.
